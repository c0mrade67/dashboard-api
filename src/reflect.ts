import 'reflect-metadata';

/* function Test(target: Function) {
    Reflect.defineMetadata('a', 1, target);
    const meta = Reflect.getMetadata('a', target);
    console.log(meta);
} */

function Inject(key: string) {
	return (target: Function): void => {
		Reflect.defineMetadata(key, 1, target);
		const meta = Reflect.getMetadata(key, target);
		console.log(meta);
	};
}

function Prop(target: Object, name: string): void {
	console.log(name);
}

@Inject('Class')
export class reflect {
	@Prop prop: number;
}
